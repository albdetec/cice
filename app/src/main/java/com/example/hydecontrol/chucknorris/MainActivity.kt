package com.example.hydecontrol.chucknorris

import android.app.DownloadManager
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gotosecondary()
    }

    private fun gotosecondary() {

     boton_categorias.setOnClickListener {

            val intent = Intent(this@MainActivity, SecondaryActivity::class.java)

            startActivity(intent)
        }
    }
}


