package com.example.hydecontrol.chucknorris

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Website.URL
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_secondary.*
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.util.Log

class SecondaryActivity: AppCompatActivity() {



    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondary)

        layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager

        adapter = RecyclerAdapter()
        recycler_view.adapter = adapter

        gotothird()

    }


    private fun gotothird() {

        fav.setOnClickListener {

            val intent = Intent(this, ThirdActivity::class.java)

            startActivity(intent)
        }

    }

}