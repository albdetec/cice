package com.example.hydecontrol.chucknorris

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.hydecontrol.chucknorris.R.attr.layoutManager
import com.example.hydecontrol.chucknorris.R.id.recycler_view
import kotlinx.android.synthetic.main.activity_third.*
import kotlinx.android.synthetic.main.activity_third.view.*
import java.net.URL
import com.google.gson.Gson

class ThirdActivity: AppCompatActivity() {

    lateinit var recycler: RecyclerView
    lateinit var boton_giveme: Button
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null

    companion object {
        fun newInstance(): ThirdActivity {
            return ThirdActivity()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)


    }
    private fun initUi(rootView: View) {

        layoutManager = LinearLayoutManager(this)
        recycler_view_third.layoutManager = layoutManager

        adapter = RecyclerAdapter()
        recycler_view_third.adapter = adapter


        boton_giveme = rootView.dame_otro
        boton_giveme.setOnClickListener { getJoke() }
    }

    private fun getJoke() {
        var result: String
        Thread {
            result = URL("https://api.chucknorris.io/jokes/categories").readText()
            runOnUiThread {
                Log.d("CICE", result)

                val joke = Gson().fromJson(result, ThirdActivity::class.java)

            }
        }.start()
    }

}



