package com.example.hydecontrol.chucknorris

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import java.net.URL
import android.util.Log
import kotlinx.android.synthetic.main.activity_secondary.view.*


class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private val categorias = arrayOf("categoria1", "categoria2", "categoria3",
        "categoria4", "categoria5","categoria6","categoria7")
    private val details = arrayOf("detalles", "detalles", "detalles",
        "detalles", "detalles", "detalles", "detalles")
    private val images = intArrayOf(R.drawable.mano, R.drawable.mano, R.drawable.mano,
        R.drawable.mano, R.drawable.mano, R.drawable.mano, R.drawable.mano)

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var itemImage: ImageView
            var itemJoke: TextView
            var itemDetail: TextView



            init {
                itemImage = itemView.findViewById(R.id.item_image)
                itemJoke = itemView.findViewById(R.id.item_title)
                itemDetail = itemView.findViewById(R.id.item_detail)
            }
        }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.card_layout_categorias, viewGroup, false)
        return ViewHolder(v)

    }



    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.itemJoke.text = categorias[i]
        viewHolder.itemDetail.text = details[i]
        viewHolder.itemImage.setImageResource(images[i])
    }

    override fun getItemCount(): Int {
        return categorias.size
    }

}