package com.example.hydecontrol.chucknorris

import android.content.res.TypedArray
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.JsonReader
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_third.*
import java.io.Reader
import java.net.URL


class RecyclerAdapterThird : RecyclerView.Adapter<RecyclerAdapterThird.ViewHolder>() {



    private val jokes = arrayOf("chiste1", "chiste2", "chiste3",
        "chiste4", "chiste5", "chiste6", "chiste7")

    private val details = arrayOf(
        "detalle", "detealle",
        "detalle", "detalle",
        "detalle", "detalle",
        "detalle"
    )

    private val images = intArrayOf(R.drawable.mano, R.drawable.mano, R.drawable.mano,
        R.drawable.mano, R.drawable.mano, R.drawable.mano, R.drawable.mano)


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemImage: ImageView
        var itemTitle: TextView
        var itemDetail: TextView

        init {
            itemImage = itemView.findViewById(R.id.item_image)
            itemTitle = itemView.findViewById(R.id.item_title)
            itemDetail = itemView.findViewById(R.id.item_detail)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.card_layout_favoritos, viewGroup, false)
        return ViewHolder(v)

    }


    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.itemTitle.text = jokes[i]
        viewHolder.itemDetail.text = details[i]
        viewHolder.itemImage.setImageResource(images[i])
    }

    override fun getItemCount(): Int {
        return jokes.size
    }

}